# PROGRAMS I USE


# Table of Contents

1.  [server](#org7f91740)
        1.  [lighttpd (http)](#orga94ff95)
        2.  [PHP](#orga36608b)
        3.  [Database (???)](#org5d176c2)
        4.  [bucktooth (gopher server)](#org5c7570d)
2.  [RSS](#org176f4fb)
        1.  [elfeed](#orga9acf26)
        2.  [moonmoon (server)](#org379bc6a)
3.  [IRC](#orgb212878)
        1.  [ERC](#orgbab05a0)
        2.  [hexchat](#orgda42468)
4.  [OS](#orge4f7a2f)
        1.  [Arch linux](#orge32c890)
        2.  [Windows](#orgbdb0f59)
5.  [PDF READER](#org86f3bc3)
        1.  [Zathura](#org0c67d11)
        2.  [Sumatra](#orgbd04512)
6.  [music player](#orgc9a4784)
        1.  [foobar2k](#orgc2a559f)
        2.  [audacious](#org05a8022)
        3.  [cmus](#orga7a0ca0)
7.  [torrent client](#orgbcb02ae)
        1.  [deluge](#orga92845e)
        2.  [qbittorent](#org2fe95fc)
8.  [video player](#orgd5c8417)
        1.  [mpv](#org291a7c5)
        2.  [vlc](#orga4d0382)
9.  [WM](#org990624e)
        1.  [openbox](#orgc951240)
10. [status bar](#orgf93fa4a)
        1.  [tint2](#org704c5e2)
11. [editor](#org925c03e)
        1.  [emacs](#org2f04337)
        2.  [textadept](#org5912cc3)
12. [data editor](#org2ad4c56)
        1.  [sc-im](#org3613fec)
        2.  [org-mode](#org2553f45)
        3.  [excel](#orgec00670)
13. [video editor](#org7bf74a8)
        1.  [ffmpeg](#org2cc0650)
        2.  [VEGAS PRO](#org4c22a98)
14. [file manager](#orgd3b775e)
        1.  [command line](#org99fd0cf)
        2.  [thunar](#org4dab50d)
        3.  [windows file manager](#orgcdae8dd)
15. [terminal emulator](#orgaabd40f)
        1.  [termite](#orgd85cc9b)
        2.  [putty](#orgcf0fa02)
16. [password manager](#org15c26a6)
        1.  [kpcli](#org4ba0435)
17. [audio editing](#orgfa43cd6)
        1.  [sox](#org71b4999)
        2.  [audacity](#orgeda6300)
18. [wallpaper](#org532cf09)
        1.  [nitrogen](#org2c4c03d)
19. [games](#org7f66b70)
    1.  [dungeon crawl stone soup](#orgb9db598)
    2.  [elona](#org69f3f85)
    3.  [infra arcana](#org79b8c31)


<a id="org7f91740"></a>

# server


<a id="orga94ff95"></a>

### lighttpd (http)

Simple, fast and lightweight. Very easy to use


<a id="orga36608b"></a>

### PHP

Extensible, very popular with many options


<a id="org5d176c2"></a>

### Database (???)


<a id="org5c7570d"></a>

### bucktooth (gopher server)


<a id="org176f4fb"></a>

# RSS


<a id="orga9acf26"></a>

### elfeed

extremely extensible, easy to use


<a id="org379bc6a"></a>

### moonmoon (server)

very simple, perhaps too simple


<a id="orgb212878"></a>

# IRC


<a id="orgbab05a0"></a>

### ERC

Easy to use, easy to copy and paste

1.  resources

    [ERC manual](https://www.gnu.org/software/emacs/manual/html_mono/erc.html)
    [emacswiki on ERC](https://www.emacswiki.org/emacs/ErcConfiguration)
    [AlexShroeder ERC](https://www.emacswiki.org/emacs/AlexSchroederErcConfig)


<a id="orgda42468"></a>

### hexchat

simple, extensible

1.  resources

    [manual](https://hexchat.readthedocs.io/en/latest/getting_started.html)


<a id="orge4f7a2f"></a>

# OS


<a id="orge32c890"></a>

### Arch linux

pretty easy to install and configurable

1.  resources

    [archwiki](https://wiki.archlinux.org/)


<a id="orgbdb0f59"></a>

### Windows

For Games and software


<a id="org86f3bc3"></a>

# PDF READER


<a id="org0c67d11"></a>

### Zathura

Simple, great keyboard

1.  resources

    [manual](https://pwmt.org/projects/zathura/documentation/)


<a id="orgbd04512"></a>

### Sumatra

easy to configure

1.  resources

    [manual](https://www.sumatrapdfreader.org/manual.html)


<a id="orgc9a4784"></a>

# music player


<a id="orgc2a559f"></a>

### foobar2k

Extremly customizable, beutiful


<a id="org05a8022"></a>

### audacious

not as good as foobar2k, simple


<a id="orga7a0ca0"></a>

### cmus

simple cli progam


<a id="orgbcb02ae"></a>

# torrent client


<a id="orga92845e"></a>

### deluge

python, mulitiple ways to use, stable


<a id="org2fe95fc"></a>

### qbittorent

qt, search feature


<a id="orgd5c8417"></a>

# video player


<a id="org291a7c5"></a>

### mpv

simple and configurable


<a id="orga4d0382"></a>

### vlc

Allows streaming, lua


<a id="org990624e"></a>

# WM


<a id="orgc951240"></a>

### openbox

simple and configurable


<a id="orgf93fa4a"></a>

# status bar


<a id="org704c5e2"></a>

### tint2

simple and configurable


<a id="org925c03e"></a>

# editor


<a id="org2f04337"></a>

### emacs

powerful, lisp


<a id="org5912cc3"></a>

### textadept

simple with lua functionalities. Able to handle largle files


<a id="org2ad4c56"></a>

# data editor


<a id="org3613fec"></a>

### sc-im

cli, can be accessed remotly


<a id="org2553f45"></a>

### org-mode

better for simple data


<a id="orgec00670"></a>

### excel

for windows


<a id="org7bf74a8"></a>

# video editor


<a id="org2cc0650"></a>

### ffmpeg

general purpose splices


<a id="org4c22a98"></a>

### VEGAS PRO


<a id="orgd3b775e"></a>

# file manager


<a id="org99fd0cf"></a>

### command line

general purpose


<a id="org4dab50d"></a>

### thunar

low effort


<a id="orgcdae8dd"></a>

### windows file manager

low effort


<a id="orgaabd40f"></a>

# terminal emulator


<a id="orgd85cc9b"></a>

### termite

vim like keys, easily customizable


<a id="orgcf0fa02"></a>

### putty

access remotely


<a id="org15c26a6"></a>

# password manager


<a id="org4ba0435"></a>

### kpcli

access remotly, simple


<a id="orgfa43cd6"></a>

# audio editing


<a id="org71b4999"></a>

### sox

simple, kinda like ffmpeg for music


<a id="orgeda6300"></a>

### audacity

simple, open source


<a id="org532cf09"></a>

# wallpaper


<a id="org2c4c03d"></a>

### nitrogen

simple with gui


<a id="org7f66b70"></a>

# games


<a id="orgb9db598"></a>

## dungeon crawl stone soup


<a id="org69f3f85"></a>

## elona


<a id="org79b8c31"></a>

## infra arcana

